package com.vr.miniautorizador.exception;

import org.springframework.http.HttpStatus;
import org.springframework.web.bind.annotation.ResponseStatus;

import java.text.MessageFormat;

@ResponseStatus(HttpStatus.UNPROCESSABLE_ENTITY)
public class CardNumberExceptionUnprocessable extends BusinessException {

    private static final long serialVersionUID = 1918184724741319997L;

    public static final String UNPROCESSABLE_ENTITY = "{1} {0} Unprocessable Entity";

    public CardNumberExceptionUnprocessable(String object, String objectDescription) {

        String description = MessageFormat.format(UNPROCESSABLE_ENTITY, object, objectDescription);

        super.setMessage(description);
        super.setDescription(description);
        super.setHttpStatusCode(HttpStatus.UNPROCESSABLE_ENTITY);
        super.setCode(String.valueOf(HttpStatus.UNPROCESSABLE_ENTITY.value()));
    }
}
