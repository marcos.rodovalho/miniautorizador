package com.vr.miniautorizador.service;

import com.vr.miniautorizador.domain.Card;
import com.vr.miniautorizador.domain.form.CardForm;
import com.vr.miniautorizador.exception.CardNumberExceptionNotFound;
import com.vr.miniautorizador.exception.CardNumberExceptionUnprocessable;
import com.vr.miniautorizador.repository.CardRepository;
import lombok.RequiredArgsConstructor;
import org.springframework.stereotype.Service;

@Service
@RequiredArgsConstructor
public class CardService {

    private final CardRepository repository;

    public Card searchByCardNumber(String cardNumber) {
        return repository.findCardByNumero(cardNumber).orElseThrow(() -> new CardNumberExceptionNotFound(cardNumber, "CARTAO_INEXISTENTE"));
    }

    public Card createCard(CardForm cardForm) {
        try {
            searchByCardNumber(cardForm.getNumeroCartao());
            throw new CardNumberExceptionUnprocessable(cardForm.getNumeroCartao(), "Cartão");
        } catch (CardNumberExceptionNotFound e) {
            Card card = Card.builder()
                    .numero(cardForm.getNumeroCartao())
                    .saldo(Double.valueOf(500))
                    .senha(cardForm.getSenha()).build();
            return repository.save(card);
        }
    }

    public Double getBalance(String numeroCartao) {
        return searchByCardNumber(numeroCartao).getSaldo();
    }
}
