package com.vr.miniautorizador.service;

import com.vr.miniautorizador.domain.Card;
import com.vr.miniautorizador.domain.form.TransactionForm;
import com.vr.miniautorizador.exception.CardNumberExceptionUnprocessable;
import com.vr.miniautorizador.repository.CardRepository;
import lombok.RequiredArgsConstructor;
import org.springframework.stereotype.Service;

import javax.transaction.Transactional;
import java.util.Optional;

@Service
@Transactional
@RequiredArgsConstructor
public class TransactionalService {

    private final CardService cardService;
    private final CardRepository cardRepository;

    @Transactional
    public String transaction(TransactionForm transactionForm) {
        Card card = cardService.searchByCardNumber(transactionForm.getNumeroCartao());
        checkPassword(transactionForm, card);
        updateValue(transactionForm, card);
        return "OK";
    }

    private void checkPassword(TransactionForm transactionForm, Card card) {
        Optional.of(transactionForm.getSenhaCartao()).filter(s -> s.equals(card.getSenha())).orElseThrow(() -> new CardNumberExceptionUnprocessable("", "SENHA_INVALIDA"));
    }

    private boolean updateValue(TransactionForm transactionForm, Card card) {
        while(card.getSaldo().compareTo(transactionForm.getValor()) >= 0) {
            card.setSaldo(card.getSaldo() - transactionForm.getValor());
            cardRepository.save(card);
            return true;
        }
        throw new CardNumberExceptionUnprocessable("", "SALDO_INSUFICIENTE");
    }
}
