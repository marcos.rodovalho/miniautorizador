package com.vr.miniautorizador.domain.form;

import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;

@Data
@Builder
@NoArgsConstructor
@AllArgsConstructor
public class TransactionForm {

    String numeroCartao;
    String senhaCartao;
    Double valor;
}
