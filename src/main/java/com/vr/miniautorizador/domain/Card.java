package com.vr.miniautorizador.domain;

import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;

import javax.persistence.*;
import java.math.BigInteger;

@Data
@Builder
@NoArgsConstructor
@AllArgsConstructor
@Entity
@Table(name = "TB_CARTAO")
public class Card {

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Basic(optional = false)
    @Column(name = "ID_CARTAO", unique = true)
    private Long id;

    @Column(name = "DS_NUMERO_CARTAO", nullable = false)
    private String numero;

    @Column(name = "DS_SENHA_CARTAO", nullable = false)
    private String senha;

    @Column(name = "VL_SALDO", nullable = false)
    private Double saldo;
}
