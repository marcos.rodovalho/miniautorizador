package com.vr.miniautorizador.controller;

import com.vr.miniautorizador.domain.form.TransactionForm;
import com.vr.miniautorizador.service.TransactionalService;
import lombok.RequiredArgsConstructor;
import lombok.extern.slf4j.Slf4j;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

@RestController
@RequestMapping("/transacoes")
@Slf4j
@RequiredArgsConstructor
public class TransactionalController {

    private final TransactionalService service;

    @PostMapping()
    @ResponseStatus(HttpStatus.CREATED)
    public ResponseEntity<String> create(@RequestBody TransactionForm transactionForm) {
        log.info("class=CardController method=create step=start");

        String data = service.transaction(transactionForm);

        log.info("class=CardController method=create step=end");

        return new ResponseEntity<>(data, HttpStatus.CREATED);
    }
}
