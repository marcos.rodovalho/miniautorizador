package com.vr.miniautorizador.controller;

import com.vr.miniautorizador.domain.Card;
import com.vr.miniautorizador.domain.form.CardForm;
import com.vr.miniautorizador.service.CardService;
import lombok.RequiredArgsConstructor;
import lombok.extern.slf4j.Slf4j;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

@RestController
@RequestMapping("/cartoes")
@Slf4j
@RequiredArgsConstructor
public class CardController {

    private final CardService service;

    @PostMapping()
    @ResponseStatus(HttpStatus.CREATED)
    public ResponseEntity<Card> create(@RequestBody CardForm cardForm) {
        log.info("class=CardController method=create step=start");

        Card data = service.createCard(cardForm);

        log.info("class=CardController method=create step=end");

        return new ResponseEntity<>(data, HttpStatus.CREATED);
    }

    @GetMapping("/{numeroCartao}")
    @ResponseStatus(HttpStatus.OK)
    public ResponseEntity<Double> getBalance(@PathVariable("numeroCartao") String numeroCartao) {
        log.info("class=CardController method=getBalance step=start");

        Double data = service.getBalance(numeroCartao);

        log.info("class=CardController method=getBalance step=end");

        return new ResponseEntity<>(data, HttpStatus.OK);
    }
}
