CREATE TABLE `TB_CARTAO`
(
    `ID_CARTAO`        int PRIMARY KEY AUTO_INCREMENT,
    `DS_NUMERO_CARTAO` varchar(50) NOT NULL,
    `DS_SENHA_CARTAO`  varchar(50) NOT NULL,
    `VL_SALDO`         int         NOT NULL
);